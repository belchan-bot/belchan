from __future__ import annotations
from typing import Optional, Set, List

from pydantic import validate_model

from .base import Field, Model, validator
from .discord import TextChannel
from ..i18n import INSTALLED_LOCALES

###############################################################################
# Main Bot Configuration


class DiscordConfig(Model):
    bot_token: str
    """ The discord bot token.

    Go to https://discord.com/developers/applications/ (Bot) for more details.
    """
    client_id: int
    """ The discord OAuth client ID for the bot.

    Go to  https://discord.com/developers/applications/ (OAuth2) for more
    details.
    """
    client_secret: str
    """ The discord OAuth client secret for the bot.

    Go to  https://discord.com/developers/applications/ (OAuth2) for more
    details.
    """
    redirect_uri: str
    """ The redirect URL for the bot.

    Must be reachable for users to work.

    Go to  https://discord.com/developers/applications/ (OAuth2) for more
    details.
    """


class DatabaseConfig(Model):
    driver: str
    """ The database driver/backend to use.

    Accepted values:
    - sqlite
    - mysql
    - postgres
    """
    filename: Optional[str] = "../instance/db.sqlite3"
    """ The filename of the database file. Only for sqlite.

    Accepted values:
    - A filename
    - ``:memory:``
    """
    host: Optional[str] = None
    username: Optional[str] = None
    password: Optional[str] = None
    database: Optional[str] = None


class WebConfig(Model):
    base_url: str
    static_path: Optional[str] = ""


class _Config(Model):
    name: Optional[str] = None
    instance_path: str = "../instance"
    secret_key: str
    discord: DiscordConfig
    database: DatabaseConfig
    web: WebConfig
    modules: List[str]
    owners: List[int]
    debug: Optional[bool] = False
    command_prefix: Optional[str] = "b!"
    translations_path: Optional[str] = "translations"

    class Config:
        case_sensitive = True


class Config(_Config):
    __singleton: Config = None

    @staticmethod
    def get_singleton() -> Config:
        return Config.__singleton

    class Config:
        case_sensitive = True


class ConfigBase(Model):
    def validate(self):
        *_, validation_error = validate_model(self.__class__, self.__dict__)

        if validation_error:
            raise validation_error


class ModuleConfig(ConfigBase):
    enabled: Optional[bool] = Field(
        True, description="Defines whether a module is enabled."
    )


class ServerConfig(ConfigBase):
    locale: Optional[str] = Field(
        "en", description="The server's locale. Used for message translation."
    )
    """The server's locale. Used for message translation.
    """
    log_channel: Optional[TextChannel] = Field(
        None,
        description="""The default channel where logs should go.

        Some modules may overwrite this with specific channels.
        """,
    )
    """The default channel where logs should go.

    Some modules may overwrite this with specific channels.
    """
    command_channels: Set[TextChannel] = Field(
        default_factory=set,
        description=""" A list of channels that the bot will allow command execution in.

        If empty, commands are accepted in all channels.
        """,
    )
    """A list of channels that the bot will allow command execution in.

    If empty, commands are accepted in all channels.
    """
    command_prefix: Optional[str] = Field(
        default_factory=lambda: Config.get_singleton().command_prefix,
        description="The prefix for commands",
    )
    """The prefix for commands.
    """


###############################################################################
# Exceptions


class MissingConfiguration(Exception):
    def __init__(self, missing_key: Optional[str] = None):
        self.missing_key = missing_key

    def __str__(self):
        if self.missing_key is not None:
            return f"Missing configuration key: {self.missing_key}."
        else:
            return "Missing configuration."
