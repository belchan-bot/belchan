from __future__ import annotations
from typing import TYPE_CHECKING, Optional, Union, Any, List, TypeVar, Type

import re
from abc import ABC, abstractmethod, abstractclassmethod
from enum import Enum

from discord import (
    TextChannel as _TextChannel,
    Role as _Role,
    User as _User,
)
from discord.ext.commands import converter


T = TypeVar("T")

if TYPE_CHECKING:
    from belchan.context import ContextBase


class Event(str, Enum):
    connect = "on_connect"
    shard_connect = "on_shard_connect"
    disconnect = "on_disconnect"
    shard_disconnect = "on_shard_disconnect"
    ready = "on_ready"
    shard_ready = "on_shard_ready"
    error = "on_error"
    socket_raw_receive = "on_socket_raw_receive"
    socket_raw_send = "on_socket_raw_send"
    typing = "on_typing"
    message_delete = "on_message_delete"
    bulk_message_delete = "on_bulk_message_delete"
    raw_message_delete = "on_raw_message_delete"
    raw_bulk_message_delete = "on_raw_bulk_message_delete"
    message_edit = "on_message_edit"
    raw_message_edit = "on_raw_message_edit"
    reaction_add = "on_reaction_add"
    raw_reaction_add = "on_raw_reaction_add"
    reaction_remove = "on_reaction_remove"
    raw_reaction_remove = "on_raw_reaction_remove"
    reaction_clear = "on_reaction_clear"
    raw_reaction_clear = "on_raw_reaction_clear"
    reaction_clear_emoji = "on_reaction_clear_emoji"
    raw_reaction_clear_emoji = "on_raw_reaction_clear_emoji"
    private_channel_create = "on_private_channel_create"
    private_channel_delete = "on_private_channel_delete"
    private_channel_update = "on_private_channel_update"
    private_channel_pins_update = "on_private_channel_pins_update"
    guild_channel_create = "on_guild_channel_create"
    guild_channel_delete = "on_guild_channel_delete"
    guild_channel_update = "on_guild_channel_update"
    guild_channel_pins_update = "on_guild_channel_pins_update"
    guild_integrations_update = "on_guild_integrations_update"
    webhooks_update = "on_webhooks_update"
    member_join = "on_member_join"
    member_remove = "on_member_remove"
    member_update = "on_member_update"
    user_update = "on_user_update"
    guild_join = "on_guild_join"
    guild_remove = "on_guild_remove"
    guild_update = "on_guild_update"
    guild_role_create = "on_guild_role_create"
    guild_role_delete = "on_guild_role_delete"
    guild_role_update = "on_guild_role_update"
    guild_emojis_update = "on_guild_emojis_update"
    guild_available = "on_guild_available"
    guild_unavailable = "on_guild_unavailable"
    voice_state_update = "on_voice_state_update"
    member_ban = "on_member_ban"
    member_unban = "on_member_unban"
    invite_create = "on_invite_create"
    invite_delete = "on_invite_delete"
    group_join = "on_group_join"
    group_remove = "on_group_remove"
    relationship_add = "on_relationship_add"
    relationship_remove = "on_relationship_remove"
    relationship_update = "on_relationship_update"

    def __str__(self) -> str:
        return self.value


ID_PATTERN: re.Pattern = re.compile(r"([0-9]{15,20})$")


def match_id(text: str) -> Optional[re.Match]:
    return ID_PATTERN.match(text)


class DiscordModel(ABC):
    @abstractclassmethod
    async def from_id(cls, ctx: ContextBase, id: int):
        pass

    @abstractmethod
    def to_id(self) -> int:
        pass

    @abstractclassmethod
    async def convert(cls: Type[T], ctx: ContextBase, argument: str) -> T:
        pass

    @abstractclassmethod
    def __get_validators__(cls):
        return


class Guild(DiscordModel):
    def __init__(self, id: Optional[int] = None, name: Optional[str] = None):
        self.id = id
        self.name = name

    @classmethod
    async def from_id(cls, ctx: ContextBase, id: int):
        if id is None:
            return cls(id=None)

        guild: _TextChannel = ctx.get_guild(id)

        if guild is None:
            return cls(id=id, name="None")

        return cls(id=guild.id, name=guild.name)

    def to_id(self):
        return self.id

    @classmethod
    async def convert(cls: Type[T], ctx: ContextBase, argument: str) -> T:
        argument = argument.replace('"', "")

        dobj = await converter.GuildConverter().convert(ctx, argument)

        return cls(id=dobj.id, name=dobj.name)

    @classmethod
    def from_str(cls: Type[T], text: str) -> T:
        pass

    @classmethod
    def __get_validators__(cls):
        yield

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(pattern="*")

    def __str__(self):
        return f"{self.name} ({self.id})" if self.id else f"{self.id}"


class TextChannel(DiscordModel):
    def __init__(self, id: Optional[int] = None, name: Optional[str] = None):
        self.id = id
        self.name = name

    @classmethod
    async def from_id(cls, ctx: ContextBase, id: int):
        if id is None:
            return cls(id=None)

        channel: _TextChannel = ctx.guild.get_channel(id)

        if channel is None:
            return cls(id=id, name="None")

        return cls(id=channel.id, name=channel.name)

    def to_id(self) -> int:
        return self.id

    @classmethod
    async def convert(cls: Type[T], ctx: ContextBase, argument: str) -> T:
        argument = argument.replace('"', "")

        dobj = await converter.TextChannelConverter().convert(ctx, argument)

        return cls(id=dobj.id, name=dobj.name)

    @classmethod
    def __get_validators__(cls):
        yield

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(pattern="*")

    def __str__(self):
        return f"#{self.name}" if self.name else f"{self.id}"


class Role(DiscordModel):
    def __init__(self, id: Optional[int] = None, name: Optional[str] = None):
        self.id = id
        self.name = name

    @classmethod
    async def from_id(cls, ctx: ContextBase, id: int):
        if id is None:
            return cls(id=None)

        role: _Role = ctx.guild.get_role(id)

        return cls(id=role.id, name=role.name)

    def to_id(self) -> int:
        return self.id

    @classmethod
    async def convert(cls: Type[T], ctx: ContextBase, argument: str) -> T:
        argument = argument.replace('"', "")

        dobj = await converter.RoleConverter().convert(ctx, argument)

        return cls(id=dobj.id, name=dobj.name)

    @classmethod
    def __get_validators__(cls):
        yield

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(pattern="*")

    def __str__(self):
        return f"{self.name} ({self.id})" if self.id else f"{self.id}"


class User(DiscordModel):
    def __init__(self, id: Optional[int] = None, name: Optional[str] = None):
        self.id = id
        self.name = name

    @classmethod
    async def from_id(cls, ctx: ContextBase, id: int):
        if id is None:
            return cls(id=None)

        user: _User = ctx.get_user(id)

        return cls(id=user.id, name=user.name)

    def to_id(self):
        return self.id

    @classmethod
    async def convert(cls: Type[T], ctx: ContextBase, argument: str) -> T:
        argument = argument.replace('"', "")

        dobj = await converter.UserConverter().convert(ctx, argument)

        return cls(id=dobj.id, name=dobj.name)

    @classmethod
    def __get_validators__(cls):
        yield

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(pattern="*")

    def __str__(self):
        if self.id:
            return f"{self.display_name} ({self.id})"
        else:
            return f"{self.id}"
