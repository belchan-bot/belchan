from __future__ import annotations
from typing import Callable, Type, Union, List, Set, Any

import datetime
import json

from peewee import (
    BooleanField,
    CompositeKey,
    DateTimeField,
    DoesNotExist,
    FloatField,
    ForeignKeyField,
    IntegerField,
    IntegrityError,
    Model as _Model,
    TextField,
    TimestampField,
)
from playhouse.hybrid import hybrid_property

from ..database import Connection, Database, make_table_name


class Model(_Model):
    """Base class for database models."""

    class Meta:
        table_function: Callable[[Type[Model]], str] = make_table_name
        database: Database = Connection.database_proxy


class LoggedError(Model):
    server_id: int = IntegerField(null=True)
    trace: str = TextField()
    timestamp: datetime.datetime = DateTimeField()


class CommandLog(Model):
    server_id: int = IntegerField()
    command: str = TextField()
    user: int = IntegerField()
    _args: str = TextField(column_name="args", null=True)
    timestamp: datetime.datetime = DateTimeField(
        default=datetime.datetime.now()
    )

    @hybrid_property
    def args(self) -> dict:
        try:
            if self.__args is None:
                self.__args = json.loads(self._args)
        except AttributeError:
            self.__args = json.loads(self._args)

        return self.__args

    @args.setter
    def args(self, value: Union[str, dict]):
        if isinstance(value, dict):
            value = json.dumps(value)

        self._args = value


class RolePermissions(Model):
    role_id: int = IntegerField(primary_key=True, unique=True)
    server_id: int = IntegerField()
    _permissions: str = TextField(column_name="permissions", default="[]")

    @hybrid_property
    def permissions(self) -> Set[str]:
        try:
            if self.__permissions is None:
                self.__permissions = set(json.loads(self._permissions or "[]"))
        except AttributeError:
            self.__permissions = set(json.loads(self._permissions or "[]"))

        return self.__permissions

    @permissions.setter
    def permissions(self, value: Union[str, json.json, List[str], Set[str]]):
        if isinstance(value, list):
            self._permissions = json.dumps(sorted(set(value)))
            self.__permissions = set(value)
        elif isinstance(value, set):
            self._permissions = json.dumps(sorted(value))
            self.__permissions = value
        elif isinstance(value, str):
            self._permissions = value
            self.__permissions = json.loads(value)
        else:
            raise TypeError(
                f"Expected one of: 'str', 'json.json', 'list' or 'set', got 'type(value)'!"
            )


class ServerConfig(Model):
    server_id: int = IntegerField(primary_key=True, unique=True)
    _config: str = TextField(column_name="config")

    @hybrid_property
    def config(self) -> dict:
        try:
            if self.__config is None:
                self.__config = json.loads(self._config)
        except AttributeError:
            self.__config = json.loads(self._config)

        return self.__config

    @config.setter
    def config(self, value: Union[str, Any]):
        if isinstance(value, dict):
            value = json.dumps(value)

        self._config = value


class ModuleConfig(Model):
    server_id: int = IntegerField()
    module: str = TextField()
    _config: str = TextField(column_name="config", default="{}")

    @hybrid_property
    def config(self) -> List[str]:
        try:
            if self.__config is None:
                self.__config = json.loads(self._config)
        except AttributeError:
            self.__config = json.loads(self._config)

        return self.__config

    @config.setter
    def config(self, value: Union[str, dict]):
        if isinstance(value, dict):
            self._config = json.dumps(value)
            self.__config = value
        else:
            self._config = value
            self.__config = json.loads(value)

    class Meta:
        primary_key: CompositeKey = CompositeKey("server_id", "module")
