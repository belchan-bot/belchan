from __future__ import annotations
from belchan.models.discord import DiscordModel
from typing import TYPE_CHECKING, Literal

import os

from dynaconf import Dynaconf, Validator, loaders

from .cache import TTLCache, cached, hashkey as cachekey
from .database import db_session, db_context
from .models.database import (
    DoesNotExist,
    ModuleConfig as DBModuleConfig,
    ServerConfig as DBServerConfig,
)
from .models.config import (
    ServerConfig,
    ModuleConfig,
    Config,
    DatabaseConfig,
    DiscordConfig,
    WebConfig,
)

if TYPE_CHECKING:
    from .application import Application


def jsonize(v):
    if isinstance(v, DiscordModel):
        return v.to_id()
    elif isinstance(v, set):
        # Remove duplicates, then convert to list
        return list(set([jsonize(e) for e in v]))
    elif isinstance(v, list):
        return [jsonize(e) for e in v]

    return v


def load_config(
    settings_files: os.PathLike,
    env: Literal["development", "production"] = "production",
) -> Config:
    dconf: Dynaconf = Dynaconf(
        envvar_prefix=False,
        environments=True,
        env=env,
        settings_files=settings_files,
        validators=[
            Validator("name", default="Belchan"),
            Validator("command_prefix", must_exist=True),
            Validator("database.driver", must_exist=True),
            Validator("discord.bot_token", must_exist=True),
            Validator("web.static_path", default="/static"),
            Validator("web.base_url", default=""),
        ],
    )

    config: Config = Config.parse_obj(
        {k.lower(): v for k, v in dconf.as_dict(env=env).items()}
    )
    Config._Config__singleton = config

    return config


def generate_config(file_name: str):
    loader_name = f"{file_name.rpartition('.')[-1]}_loader"
    loader = loaders.__dict__.get(loader_name)

    if not loader:
        raise IOError(f"{loader_name} cannot be found.")

    data = {}

    if loader is not loaders.__dict__.get("python_loader"):
        for env in ["production", "development"]:
            data.update(
                {
                    env: Config.construct(
                        name="Belchan",
                        command_prefix="b!",
                        instance_path=os.path.relpath(
                            os.path.join(os.path.dirname(__file__), "..")
                        ),
                        owners=["INSERT YOUR ID HERE"],
                        secret_key="",
                        modules=[
                            "config",
                            "general",
                            "admin",
                            "log",
                            "reaction_roles",
                            "fun",
                        ],
                        database=DatabaseConfig.construct(driver="sqlite"),
                        discord=DiscordConfig.construct(
                            bot_token="INSERT TOKEN HERE",
                            client_id="INSERT CLIENT ID HERE",
                            client_secret="INSERT CLIENT SECRET HERE",
                            redirect_uri="INSERT REDIRECT URI HERE",
                        ),
                        web=WebConfig.construct(
                            base_url="/", static_path="/static"
                        ),
                    ).dict()
                }
            )

    loader.write(file_name, data, merge=False)


###############################################################################
# Runtime configuration


class ConfigCache:
    module_config_cache: TTLCache = None
    server_config_cache: TTLCache = None


@cached(cache=ConfigCache.module_config_cache)
def get_module_config(
    app: Application, server_id: int, module: str
) -> ModuleConfig:
    try:
        db_entry: DBModuleConfig = DBModuleConfig.get(
            server_id=server_id, module=module.lower()
        )

        try:
            # Return a configuration with values from the
            return app._module_config_templates[module].from_dict(
                db_entry.config
            )
        except KeyError:
            # Module has no registred config template.
            return ModuleConfig()
    except DoesNotExist:
        try:
            # Return a configuration with default values.
            return app._module_config_templates[module]()
        except KeyError:
            # Module has no registred config template.
            return ModuleConfig()


async def update_module_config(
    server_id: int, module: str, config: DBModuleConfig
):
    """Update the configuration for the specified server and module."""

    json_cfg = {k: jsonize(v) for k, v in config.dict().items()}

    async with db_context():
        # Update the database entry for the server and module.
        db_entry = None

        try:
            db_entry = DBModuleConfig.get(
                server_id=server_id, module=module.lower()
            )
        except DoesNotExist:
            db_entry = DBModuleConfig.create(
                server_id=server_id, module=module, config=json_cfg
            )

        db_entry.config = json_cfg
        db_entry.save()

    # Clear old config from cache.
    try:
        del ConfigCache.module_config_cache[cachekey(server_id, module)]
    except KeyError:
        pass


@db_session
def get_server_config(server_id: int) -> ServerConfig:
    try:
        db_entry = DBServerConfig.get(server_id=server_id)

        return ServerConfig(
            locale=db_entry.config["locale"],
            log_channel=db_entry.config["log_channel"],
            command_channels=db_entry.config["command_channels"],
        )
    except DoesNotExist:
        return ServerConfig()


async def update_server_config(server_id: int, config: DBServerConfig):
    """Update the configuration for the specified server."""

    json_cfg = {k: jsonize(v) for k, v in config.dict().items()}

    async with db_context():
        db_entry = None

        try:
            db_entry = DBServerConfig.get(server_id=server_id)
            db_entry.config = json_cfg
            db_entry.save()
        except DoesNotExist:
            db_entry = DBServerConfig.create(
                server_id=server_id, config=config.json()
            )
