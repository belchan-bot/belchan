"""Context related functions and classes.

Contexts are used for passing information like the channel and server where a
command was given and which member gave it.
"""
from typing import Optional

from abc import ABC, abstractproperty

from discord.ext import commands

from .models.config import ModuleConfig, ServerConfig


class ContextBase(commands.Context, ABC):
    """The abstract class/interface for all contexts."""

    @abstractproperty
    def locale(self) -> str:
        """The locale (language) of the current context."""
        pass

    @abstractproperty
    def module_config(self) -> Optional[ModuleConfig]:
        """The configuration of the module the command was"""
        pass

    @abstractproperty
    def server_config(self) -> ServerConfig:
        pass

    @abstractproperty
    def vars(self) -> dict:
        pass


class ContextServerConfigMixin:
    @property
    def server_config(self) -> ServerConfig:
        if not hasattr(self, "__server_config__"):
            self.__server__config = self.bot.get_server_config(self.guild.id)

        return self.__server__config


class ContextModuleConfigMixin:
    @property
    def module_config(self) -> Optional[ModuleConfig]:
        if not hasattr(self, "__module_config"):
            if self.command.cog_name:
                self.__module_config = self.bot.get_module_config(
                    self.guild.id, self.command.cog_name
                )

            return self.__module_config
        else:
            self.__module_config == None

        return self.__module_config


class ContextLocaleMixin:
    def __init__(self, **attrs):
        super().__init__(**attrs)

        self.__locale: str = self.bot.get_locale(self.guild)

    @property
    def locale(self) -> str:
        return self.__locale


class ContextVarsMixin:
    def __init__(self, **attrs):
        super().__init__(**attrs)

        self.__vars = dict()

    @property
    def vars(self) -> dict:
        return self.__vars


class Context(
    ContextModuleConfigMixin,
    ContextServerConfigMixin,
    ContextLocaleMixin,
    ContextVarsMixin,
    ContextBase,
):
    """Default context for belchan.

    Attributes:
        module_config: The configuration for module used in the current context.
        server_config: The configuration for server used in the current context.
        vars: A storage
    """

    slots = (
        "args",
        "author",
        "bot",
        "channel",
        "command",
        "command_failed",
        "guild",
        "invoked_parents",
        "invoked_subcommands",
        "invoked_with",
        "kwargs",
        "me",
        "message",
        "prefix",
        "subcommand_passed",
        "valid",
        "voice_client",
        "__server_config",
        "__module_config",
        "__locale",
        "__vars",
    )

    def __init__(self, **attrs):
        super().__init__(**attrs)
