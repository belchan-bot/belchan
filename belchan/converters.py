from __future__ import annotations
from typing import (
    TYPE_CHECKING,
    TypeVar,
    Generic,
    get_args,
    Union,
    Optional,
    Dict,
)

import datetime

import discord
import babel.dates
import dateutil.parser
import pytimeparse
from discord.ext.commands import Converter, BadArgument

from .context import ContextBase

from .i18n import _

T = TypeVar("T")

if TYPE_CHECKING:
    from .application import Application


locale_parsers: Dict[str, dateutil.parser.parserinfo] = {}


def get_locale_parser(locale: str) -> dateutil.parser.parserinfo:
    try:
        return locale_parsers[locale]
    except KeyError:
        months_wide = babel.dates.get_month_names(width="wide", locale=locale)
        months_3 = babel.dates.get_month_names(
            width="abbreviated", locale=locale
        )
        days_wide = babel.dates.get_day_names(width="wide", locale=locale)
        days_3 = babel.dates.get_day_names(width="abbreviated", locale=locale)
        periods = babel.dates.get_period_names(locale=locale)

        class localized_parser(dateutil.parser.parserinfo):
            APMPM = [periods["am"], periods["pm"]]
            MONTHS = [
                (str(months_wide[i]), str(months_3[i]))
                for i in range(
                    1, len(months_wide)
                )  # Months start counting at 1
            ]
            WEEKDAYS = [
                (str(days_wide[i]), str(days_3[i]))
                for i in range(
                    0, len(days_wide)
                )  # Weekdays start counting at 0
            ]

        parser: localized_parser = localized_parser()

        locale_parsers[locale] = parser

        return parser


async def to_server(
    app: Application, server: Union[discord.Guild, int]
) -> discord.Guild:
    if isinstance(server, discord.Guild):
        return server
    else:
        server = app.get_guild(server)

        return server


async def to_user_or_member(
    app: Application,
    user: Union[discord.User, discord.Member, int],
    server: Optional[discord.Guild] = None,
) -> Union[discord.User, discord.Member]:
    if isinstance(user, discord.User) or isinstance(user, discord.Member):
        return user

    if server is None:
        return app.get_user(user)
    else:
        try:
            return await server.fetch_member(user)
        except (discord.Forbidden, discord.HTTPException):
            try:
                return server.get_member(user)
            except (discord.Forbidden, discord.HTTPException):
                return app.get_user(user)


async def to_channel(
    app: Application,
    server: Union[discord.Guild, int],
    channel: Union[discord.TextChannel, int],
) -> discord.TextChannel:
    if isinstance(channel, discord.TextChannel):
        return channel

    server = await to_server(app, server)

    return server.get_channel(channel)


async def to_role(
    app: Application,
    role: Union[discord.Role, int],
    server: Union[discord.Guild, int],
):
    if isinstance(role, discord.Role):
        return role

    server = await to_server(app, server)

    return server.get_role(role)


class Duration(Converter):
    async def convert(
        self, ctx: ContextBase, argument: str
    ) -> datetime.timedelta:
        argument = argument.lower()

        delta = pytimeparse.parse(argument)

        if delta is not None:
            return datetime.timedelta(seconds=delta)

        raise BadArgument(
            _("Invalid duration: {duration}", locale=ctx.locale).format(
                duration=argument
            )
        )


class DateTime(Converter):
    async def convert(
        self, ctx: ContextBase, argument: str
    ) -> datetime.datetime:
        try:
            return dateutil.parser.parse(
                argument, parserinfo=get_locale_parser(ctx.locale)
            )
        except dateutil.parser._parser.ParserError:
            raise BadArgument(
                _("Invalid date-time: {duration}", ctx.locale).format(
                    duration=argument
                )
            )


class Enum(Generic[T], Converter):
    def convert(self, ctx: ContextBase, argument: str) -> T:
        return get_args(self.__orig_class__)[0](argument)
