from pyfakefs.fake_filesystem import FakeFilesystem

from .config import Config
from . import fs

import pathlib


class TestFileTypeMatcher:
    def test_file_matches(self, mock_fs: FakeFilesystem):
        assert fs.FileType.text.file_matches("test.txt")
        assert not fs.FileType.text.file_matches("test.wrong")

        assert fs.FileType.any.file_matches("test.txt")

    def test_list_matches(self, mock_fs: FakeFilesystem):
        mock_fs.create_file("match1.txt")
        mock_fs.create_file("test.wrong")
        mock_fs.create_file("match2.txt")

        all_files = fs.FileType.any.list_file_matches(pathlib.Path(mock_fs.cwd))
        txt_files = fs.FileType.text.list_file_matches(
            pathlib.Path(mock_fs.cwd)
        )
        image_files = fs.FileType.image.list_file_matches(
            pathlib.Path(mock_fs.cwd)
        )

        assert len(all_files) == 3
        assert len(txt_files) == 2
        assert len(image_files) == 0


class TestBundle:
    def test_create(self, mock_fs: FakeFilesystem):
        pass
