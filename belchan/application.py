from __future__ import annotations
from typing import (
    TYPE_CHECKING,
    Any,
    Awaitable,
    Callable,
    Dict,
    List,
    NoReturn,
    Tuple,
    Type,
    TypeVar,
    Union,
    Optional,
)
from abc import ABC, abstractclassmethod, abstractmethod, abstractproperty

import asyncio
import os
import pathlib
import traceback

import logging
from pathlib import Path

import discord
from discord.ext import commands
from discord.ext.commands.errors import CheckFailure

if TYPE_CHECKING:
    from .modules import Module
    from .models.config import ModuleConfig
    from .models.permissions import ModulePermission
    from .config import ConfigBase

from .cache import TTLCache
from .config import Config, ConfigCache
from .config import get_module_config as _get_module_config
from .config import get_server_config as _get_server_config
from .config import update_module_config as _update_module_config
from .config import update_server_config as _update_server_config
from .context import Context
from .database import Connection
from .database import DoesNotExist as DBNotFound
from .fs import Bundle
from .i18n import _, install_locale, Catalog, INSTALLED_LOCALES
from .models.config import MissingConfiguration, ServerConfig
from .models.database import CommandLog as DBCommandLog
from .models.database import ModuleConfig as DBModuleConfig
from .models.database import RolePermissions as DBRolePermissions
from .models.database import ServerConfig as DBServerConfig
from .models.docs import ModuleDocumentation

T = TypeVar("T")


def await_function(func: Callable[..., Awaitable[T]], *args, **kwargs) -> T:
    """Runs the given async function with the given arguments."""

    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(func(*args, **kwargs))
    loop.run_until_complete(asyncio.wait(future))
    loop.close()

    return future.result()


class Application(ABC):
    @abstractmethod
    def run(self, token: str):
        pass

    @abstractmethod
    def get_locale(self, server: discord.Guild) -> str:
        pass

    @abstractmethod
    def add_module(self, module: Module):
        pass

    @abstractmethod
    def get_module_config(self, server_id: int, module: str) -> ModuleConfig:
        pass

    @abstractmethod
    async def update_module_config(
        self, server_id: int, module: str, config: ModuleConfig
    ):
        pass

    @abstractmethod
    def remove_module(self, module: str):
        pass

    @abstractmethod
    async def get_command_doc(
        self, command: str, ctx: Context
    ) -> commands.Paginator:
        pass

    @abstractmethod
    async def show_help_for(self, ctx: Context, topic: str):
        pass

    @abstractmethod
    def load_module(self, name: str):
        pass

    @abstractmethod
    def unload_module(self, name: str):
        pass

    @abstractmethod
    async def handle_error(self, ctx: Context, exception: Exception):
        pass

    @abstractmethod
    async def get_context(self, message, *, cls: Type[T] = Context):
        pass

    @abstractproperty
    def database(self):
        pass

    @abstractproperty
    def module_documentations(self) -> Dict[str, ModuleDocumentation]:
        pass

    @abstractclassmethod
    def get_singleton(cls: Application) -> Application:
        pass


class __Application:
    logger: logging.Logger = None
    module_path: str = ""
    root_bundle: Bundle = None

    __singleton: Application = None
    _module_config_templates: Dict[str, Type[ModuleConfig]] = {}
    data: Dict[str, Any] = {}

    def __init__(
        self,
        config: Union[Config, str],
        loop: Optional[asyncio.AbstractEventLoop] = None,
    ):
        Application.__singleton = self
        ConfigCache.module_config_cache = TTLCache(maxsize=1024, ttl=600)

        self.config = Config.get_singleton()

        #######################################################################
        # Configuration

        self.module_path = os.path.abspath(
            os.path.join(os.path.dirname(__file__), "..")
        )
        self.server_configs: Dict[int, ServerConfig] = {}
        self.permissions: List[ModulePermission] = []

        locales = [
            f.stem
            for f in (Path(__file__).parent / "translations").glob("*.toml")
        ]

        for locale in locales:
            install_locale(locale)

        #######################################################################
        # Logging setup

        self.logger = logging.getLogger("belchan")
        self.logger.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(
            logging.Formatter("[%(levelname)s] %(asctime)s: %(message)s")
        )
        self.logger.addHandler(ch)

        self.logger.info("==== Start: Startup")

        #######################################################################
        # Database connection

        self.__database: Connection = Connection()
        self.__database.connect(self.config)

        self.__database.ensure_tables(
            [DBModuleConfig, DBRolePermissions, DBCommandLog, DBServerConfig]
        )

        #######################################################################
        # Filesystem

        self.root_bundle = Bundle(
            "root", pathlib.Path(self.config.instance_path, "assets")
        )
        self.root_bundle.ensure()

        #######################################################################
        # Bot startup

        super().__init__(
            command_prefix=prefix_for,
            case_insensitive=True,
            intents=discord.Intents.all(),
            loop=loop,
        )

        self.owner_ids = self.config.owners

        # Remove discord.py's builtin help command. It's useless for this bot.
        self.remove_command("help")

        # Load all modules specified in the config.
        for module in self.config.modules:
            self.load_module(module)

        self.logger.info("Establishing connection to Discord API")

    def run(self, token):
        super().run(token)

    def get_locale(self, server: discord.Guild):
        try:
            return self.server_configs[server.id].locale
        except (KeyError, AttributeError):
            return "en"

    def add_module(self, module: Module):
        self.add_cog(module)

        for p in module.__permissions__:
            if p.name in [p.name for p in self.permissions]:
                pass
            else:
                self.permissions.append(p)

        # Add the config template from the loaded module to the list of
        # config templates.
        self._module_config_templates[
            module.qualified_name.lower()
        ] = module.__config_template__

        for locale in module.list_locales():
            # Add the translations from the module to the "messages" catalogs
            # of any matching installed language.
            INSTALLED_LOCALES[locale.stem].catalogs["messages"].merge(
                Catalog.load(locale)
            )

    def get_module_config(self, server_id: int, module: str) -> ModuleConfig:
        """Returns the configuration for the specified server and module."""
        try:
            return _get_module_config(self, server_id, module.lower())
        except DBNotFound:
            return None

    async def update_module_config(
        self, server_id: int, module: str, config: ModuleConfig
    ):
        """Updatess the configuration for the specified server and module."""
        return await _update_module_config(server_id, module.lower(), config)

    def get_server_config(self, server_id: int) -> ServerConfig:
        return self.server_configs.get(server_id, ServerConfig())

    async def update_server_config(self, server_id: int, config: ServerConfig):
        await _update_server_config(server_id, config)

        self.server_configs[server_id] = config

    def remove_module(self, module: str):
        self.remove_cog(module)

    async def get_command_doc(
        self, command: str, ctx: Context
    ) -> commands.Paginator:
        """Returns the localized docstring for the requested command.

        Since modules cannot see each other, but all can see the bot, which in
        turn knows all loaded modules, this command has to be used.

        Params:
            command: The name of the command to search for.
            locale: The locale to translate the docstring to.

        Raises:
            commands.CommandNotFound: If command does not exist.
        """
        command = self.get_command(command)

        if command is not None:
            return await command.make_help(ctx)
        else:
            raise commands.errors.CommandNotFound()

    async def show_help_for(self, ctx: Context, topic: str):
        await self.get_command("help")(ctx, topic)

    def load_module(self, name: str):
        """Loads a module."""
        self.logger.info(f"-- START: Loading module {name}")

        try:
            self.load_extension(f"belchan.modules.{name}")

            self.logger.info(f"-- END: Loading module {name}")
        except commands.errors.ExtensionNotFound:
            self.logger.error("Module does not exist.")
            self.logger.info(f"-- END: Loading module {name}")

            raise
        except commands.errors.ExtensionAlreadyLoaded:
            self.logger.error("Module already loaded.")
            self.logger.info(f"-- END: Loading module {name}")

            raise
        except commands.errors.ExtensionFailed as err:
            self.logger.error(err)
            self.logger.info(f"-- END: Loading module {name}")

            raise err

    def unload_module(self, name: str):
        """Unloads a module."""
        self.logger.info(f"-- START: Unloading module {name}")

        try:
            self.unload_extension(f"belchan.modules.{name}")

            self.logger.info(f"-- END: Unloading module {name}")
        except commands.errors.ExtensionNotLoaded:
            self.logger.error("Module is not loaded.")
            self.logger.info(f"-- END: Unloading module {name}")

            raise

    def reload_module(self, name: str):
        """Reloads a module."""
        self.logger.info(f"-- START: Reloading module {name}")

        try:
            self.reload_extension(f"belchan.modules.{name}")

            self.logger.info(f"-- END: Reloading module {name}")
        except commands.errors.ExtensionNotLoaded:
            self.logger.error("Module is not loaded.")
            self.logger.info(f"-- END: Reloading module {name}")

            raise
        except commands.errors.ExtensionNotFound:
            self.logger.error("Module does not exist.")
            self.logger.info(f"-- END: Reloading module {name}")

            raise
        except commands.errors.ExtensionFailed as err:
            self.logger.error(err)
            self.logger.info(f"-- END: Reloading module {name}")

    async def on_command_error(self, ctx: Context, exception: Exception):
        """Handles errors that happen during commands that aren't caught."""
        # Do not handle errors for cogs that have their own error handler.
        if ctx.cog:
            if (
                ctx.cog._get_overridden_method(ctx.cog.cog_command_error)
                is not None
            ):
                return

        await self.handle_error(ctx, exception)

    async def handle_error(self, ctx: Context, exception: Exception):
        if isinstance(exception, commands.errors.CommandNotFound):
            await ctx.message.channel.send(
                _(
                    "The command you're trying to give does not exist.",
                    ctx.locale,
                )
            )
        elif isinstance(exception, commands.errors.MissingPermissions):
            await ctx.channel.send(
                _(
                    "You do not have the required permissions to run this"
                    " command.",
                    ctx.locale,
                )
            )
        elif isinstance(exception, commands.errors.CheckFailure):
            await ctx.channel.send(exception)
        elif isinstance(exception, commands.MissingRequiredArgument):
            await ctx.channel.send(
                _("Missing required command argument: `{}`", ctx.locale).format(
                    exception.param.name
                )
            )

            if ctx.invoked_subcommand:
                pg: commands.Paginator = await ctx.invoked_subcommand.make_help(
                    ctx
                )

                for page in pg.pages:
                    await ctx.send(page)
            else:
                await self.get_command("help")(ctx, topic=ctx.command.name)
        elif isinstance(exception, commands.errors.UserInputError):
            await ctx.channel.send(exception)
        elif isinstance(exception, MissingConfiguration):
            await ctx.channel.send(
                _(
                    "Cannot execute command due to missing configuration:"
                    " `{module}.{err.missing_key}`",
                    ctx.locale,
                ).format(module=ctx.cog.qualified_name, err=exception)
            )
        elif isinstance(exception, commands.errors.CommandInvokeError):
            await self.handle_error(ctx, exception.original)
        elif isinstance(exception, commands.errors.ConversionError):
            required_params: List[str] = list(ctx.command.params.keys())

            await ctx.channel.send(
                _(
                    "Wrong value for argument `{argument}`.",
                    locale=ctx.locale,
                ).format(argument=required_params[len(ctx.args)]),
            )

            if ctx.invoked_subcommand:
                pg: commands.Paginator = await ctx.invoked_subcommand.make_help(
                    ctx
                )

                for page in pg.pages:
                    await ctx.send(page)
            else:
                await self.get_command("help")(ctx, topic=ctx.command.name)
        else:
            await ctx.channel.send(
                _(
                    "Something went horribly wrong while trying to run your"
                    " command.",
                    ctx.locale,
                )
            )

            exc: str = "\n".join(
                traceback.format_exception(
                    type(exception), exception, exception.__traceback__
                )
            )

            self.logger.error(exc)

        # TODO : handle argument conversion and errors, so they can be
        # localized.

    async def on_shard_connect(self, shard_id: int):
        self.logger.info(f"(SHARD {shard_id}): connected.")

    async def on_shard_disconnect(self, shard_id: int):
        self.logger.info(f"(SHARD {shard_id}): disconnected.")

    async def on_shard_resumed(self, shard_id: int):
        self.logger.info(f"(SHARD {shard_id}): resumed.")

    async def on_shard_ready(self, shard_id: int):
        self.logger.info(f"(SHARD {shard_id}): ready.")

    async def on_guild_join(self, guild: discord.Guild):
        """Guild setup hook."""
        self.logger.info(f"-- START: Joining Server: {guild.name} ({guild.id})")
        self.logger.info("- Loading configuration")

        self.server_configs[guild.id] = _get_server_config(guild.id)

        self.logger.info("- Done loading configuration")
        self.logger.info(f"-- END: Joining Server: {guild.name} ({guild.id})")

    async def on_ready(self):
        """Automatically runs when the connection to discord has been
        established.
        """
        self.logger.info(f"Logged in as {self.user.name} ({self.user.id})")
        self.logger.info("-- START: Loading server configurations")

        for guild in self.guilds:
            self.logger.info(f"- {guild.name} ({guild.id})")
            self.server_configs[guild.id] = _get_server_config(guild.id)

        self.logger.info("-- END: Loading server configurations")

        self.logger.info("==== END: Startup")

        # Setup command channel restrictions check.
        @self.check
        async def __channel_restriction(ctx: Context):
            channel_restrictions: List[int] = ctx.bot.server_configs[
                ctx.guild.id
            ].command_channels

            if (
                len(channel_restrictions) > 0
                and ctx.channel.id not in channel_restrictions
            ):
                raise CheckFailure(
                    _(
                        "Commands are not allowed in this channel.",
                        locale=ctx.locale,
                    )
                )

            return True

    async def get_context(self, message, *, cls=Context):
        """Hook that injects the custom context."""
        return await super().get_context(message, cls=cls)

    @property
    def database(self):
        return self.__database

    @property
    def module_documentations(self) -> Dict[str, ModuleDocumentation]:
        return {}

    @classmethod
    def get_singleton(cls: Application) -> Application:
        if cls.__singleton is None:
            cls.__singleton = Application()

        return cls.__singleton


def make_application(cls: Type[T] = commands.AutoShardedBot):
    t = type(
        "Application",
        (__Application, cls),
        __Application.__dict__.copy(),
    )

    return t


def prefix_for(bot: Application, msg: discord.Message):
    """Return the command prefix for the current message."""

    if msg.guild:
        try:
            return bot.server_configs[msg.guild.id].command_prefix
        except KeyError:
            return bot.config.command_prefix
    else:
        return bot.config.command_prefix
