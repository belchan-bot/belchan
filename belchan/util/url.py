from typing import Dict, List, Optional, Union

from urllib.parse import urlencode

from slugify import slugify as _slugify


def make_url(
    request: Dict[str, str],
    base_path: Union[str, List[str]],
    args: Optional[Dict[str, str]] = {},
    request_args: Optional[bool] = False,
) -> str:
    """Generate a formatted URL for the given base_path and arguments.

    Params:
        base_path: The base path of the URL.
        args: A dict of parameter-key and -value.
        request_args: If true, the arguments of the current request will be
        included.
    """

    if request_args:
        args.update(request.args)
    else:
        args["lang"] = request.args.get("lang", "en")

    if isinstance(base_path, list):
        return (
            "/"
            + "/".join([str(e) for e in base_path])
            + (("?" + urlencode(args)) if len(args) > 0 else "")
        )
    else:
        return (base_path or "/") + (
            ("?" + urlencode(args)) if len(args) > 0 else ""
        )


def make_url_proxy(request):
    def make_url_proxy(
        base_path: Union[str, List[str]],
        args: Optional[Dict[str, str]] = {},
        request_args: Optional[bool] = False,
    ):
        return make_url(request, base_path, args, request_args)

    return make_url_proxy


def slugify(text: str) -> str:
    """Returns a slugified version of the text for use in URLs."""
    return _slugify(text)
