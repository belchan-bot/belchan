from io import BytesIO

import aiohttp
from werkzeug.datastructures import FileStorage


async def download_file(url: str) -> FileStorage:
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            buf: BytesIO = BytesIO(await resp.read())

            return FileStorage(stream=buf, filename=url.split("/")[-1])
