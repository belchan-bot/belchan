from typing import List, Optional, Tuple, Union, Set, Sequence

from discord.ext.commands.errors import CommandInvokeError, CommandNotFound

from belchan.application import (
    Application,
    discord,
)
from belchan import commands as dcommands
from belchan.context import ContextBase
from belchan.modules import Module
from belchan.models.discord import Role, TextChannel
from belchan.models.config import ModuleConfig, ServerConfig
from belchan.models import Field
from belchan.models.permissions import ModulePermission
from belchan.models.docs import (
    CommandDocumentation,
    ArgumentDocumentation,
    GroupDocumentation,
)
from belchan.permissions import (
    require_permission,
)
from belchan.converters import DateTime, Duration
from belchan.util import fs
from belchan.i18n import _, lazy_gettext as N_

from . import commands as mod_commands
from .commands import FileStorage


def recurse_commands(
    cmd: Union[dcommands.Command, dcommands.Group], rest: Sequence[str]
) -> dcommands.Command:
    """Recursively goes through subcommands until ``rest`` is used up.

    Args:
        cmd: The current base to search for subcommands in.
        rest: The search sequence. One entry per subcommand group or command.
    """
    if isinstance(cmd, dcommands.Group):
        if len(rest) > 1:
            return recurse_commands(cmd.get_command(rest[0]), rest[1:])
        else:
            return cmd.get_command(rest[0])


class GeneralConfig(ModuleConfig):
    iam_roles: Optional[Set[Role]] = Field(
        default_factory=set,
        description=(
            "A list of roles that users can assign to themselves"
            " using the iam command"
        ),
    )

    greet_message: Optional[str] = None
    greet_message_enabled: Optional[bool] = False
    greet_message_channel: Optional[TextChannel] = None
    greet_dm: Optional[str] = None
    greet_dm_enabled: Optional[bool] = False

    leave_message: Optional[str] = None
    leave_message_enabled: Optional[bool] = False


class General(
    Module,
    doc=N_("General commands."),
    config=GeneralConfig,
    permissions=[
        ModulePermission(
            name="say",
            description=N_("Make the bot say something."),
        )
    ],
):
    @dcommands.command(
        doc=CommandDocumentation(
            command="avatar",
            doc=N_("Show the avatar of a user."),
            args=[
                ArgumentDocumentation(
                    name="user",
                    doc=N_(
                        "The user whose avatar you want to see. Defaults"
                        " to the user giving the command."
                    ),
                    optional=True,
                ),
            ],
        )
    )
    async def avatar(
        self, ctx: ContextBase, user: Optional[discord.User] = None
    ):
        if user is None:
            user = ctx.author

        embed = discord.Embed(
            title=(_("%s's Avatar", locale=ctx.locale) % user.display_name)
        )
        embed.set_image(url=user.avatar_url)

        await ctx.channel.send(embed=embed)

    @dcommands.command(
        doc=CommandDocumentation(
            command="edit",
            doc=N_("Edit a message sent by the bot."),
            args=[
                ArgumentDocumentation(
                    name="channel",
                    doc=N_(
                        "The channel the channel that should be edited is in."
                    ),
                ),
                ArgumentDocumentation(
                    name="message",
                    doc=N_("The ID of the message that should be edited."),
                ),
                ArgumentDocumentation(
                    name="new_text",
                    doc=N_("The new text for the message."),
                ),
            ],
        )
    )
    @require_permission("edit")
    async def edit(
        self,
        ctx: ContextBase,
        channel: Union[discord.TextChannel],
        message: int,
        *,
        new_text: str,
    ):
        status = await ctx.channel.send(_("Searching...", locale=ctx.locale))

        try:
            await mod_commands.edit(channel, message, new_text)

            await status.edit(
                content=_(
                    "Successfully edited message {}", locale=ctx.locale
                ).format(message.id)
            )
        except discord.NotFound:
            await status.edit(
                content=_(
                    "Failed to find the specified message.", locale=ctx.locale
                )
            )
        except discord.Forbidden:
            await status.edit(
                content=_(
                    "Missing permissions to find the specified message.",
                    locale=ctx.locale,
                )
            )
        except discord.HTTPException:
            await status.edit(content=_("Unknown error.", locale=ctx.locale))

    @dcommands.command(
        doc=CommandDocumentation(
            command="help",
            doc=N_(
                "Show help for a command or category, or an overview of"
                " all available commands and categories."
            ),
            args=[
                ArgumentDocumentation(
                    name="topic",
                    doc=N_("The command or category to display help for."),
                    optional=True,
                ),
            ],
        ),
    )
    async def help(self, ctx: ContextBase, *, topic: Optional[str] = None):
        if topic is None:
            paginator = dcommands.Paginator(prefix="```apache")

            # Display the short help for all modules, sorted by module name.
            for name in sorted(ctx.bot.cogs):
                module: Module = ctx.bot.cogs[name]

                for line in (await module.make_short_help(ctx)).splitlines():
                    paginator.add_line(line)

            for page in paginator.pages:
                await ctx.channel.send(page)

            append: str = _(
                "Type `{prefix}help command` for more info on a specific"
                " command.\\n"
                "You can also type `{prefix}help Category` for more info on a"
                " category.",
                locale=ctx.locale,
            ).format(prefix=ctx.prefix)

            await ctx.channel.send(append)
        elif topic[0].isupper():
            # If the first character is uppercase, category help was requested.
            try:
                ret: Tuple[
                    dcommands.Paginator, dcommands.Paginator
                ] = await ctx.bot.cogs[topic].make_help(ctx)

                header, cmds = ret

                for page in header.pages:
                    await ctx.channel.send(page)

                for page in cmds.pages:
                    await ctx.channel.send(page)
            except KeyError:
                await ctx.channel.send(
                    _(
                        "No help found for the requested category.",
                        locale=ctx.locale,
                    )
                )
        else:
            exp_topic: List[str] = topic.split()

            # Handle subcommands.
            if len(exp_topic) > 1:
                cmd: Union[
                    dcommands.Command, dcommands.Group
                ] = self.bot.get_command(exp_topic[0])

                match = recurse_commands(cmd, exp_topic[1:])

                try:
                    ret: dcommands.Paginator = await match.make_help(
                        ctx, parents=exp_topic[:-1]
                    )
                except AttributeError:
                    return await ctx.channel.send(
                        _(
                            "The requested command does not exist.",
                            locale=ctx.locale,
                        )
                    )

                for page in ret.pages:
                    await ctx.channel.send(str(page))

                return

            try:
                ret: dcommands.Paginator = await ctx.bot.get_command_doc(
                    topic, ctx
                )

                for page in ret.pages:
                    await ctx.channel.send(str(page))
            except CommandNotFound:
                await ctx.channel.send(
                    _(
                        "The requested command does not exist.",
                        locale=ctx.locale,
                    )
                )

    @dcommands.command(
        doc=CommandDocumentation(
            command="say",
            doc=N_("Make the bot send a message."),
            args=[
                ArgumentDocumentation(
                    name="channel",
                    doc=N_("The channel to send the message to."),
                    optional=True,
                ),
                ArgumentDocumentation(name="text", doc=N_("The text to send.")),
            ],
        )
    )
    @require_permission("say")
    async def say(
        self,
        ctx: ContextBase,
        channel: Optional[discord.TextChannel] = None,
        *,
        text: str = None,
    ):
        if channel is None:
            channel = ctx.channel

        files: List[FileStorage] = []

        for attachment in ctx.message.attachments:
            files.append(await fs.download_file(attachment.url))

        await mod_commands.say(ctx.bot, ctx.guild, channel, text, files)
        await ctx.send(_("Message has been sent.", locale=ctx.locale))

    @dcommands.command(
        doc=CommandDocumentation(
            command="userinfo",
            aliases=["who", "whois"],
            doc=N_("Show a user's profile."),
            args=[
                ArgumentDocumentation(
                    name="user",
                    doc=N_(
                        "The user to display. Defaults to the user giving"
                        " the command."
                    ),
                    optional=True,
                ),
            ],
        ),
        aliases=["whois", "who"],
    )
    async def userinfo(
        self,
        ctx: ContextBase,
        user: Optional[Union[discord.Member, discord.User]] = None,
    ):
        if user is None:
            user = ctx.message.author

        embed = discord.Embed(title=f"{user.display_name}")
        embed.set_thumbnail(url=user.avatar_url)
        embed.add_field(name="ID", value=user.id, inline=False)

        if isinstance(user, discord.Member):
            embed.add_field(
                name=_("Nickname", locale=ctx.locale),
                value=user.nick,
                inline=True,
            )
            embed.add_field(
                name=_("Status", locale=ctx.locale),
                value=user.status,
                inline=True,
            )

        embed.add_field(
            name=_("Account creation date", locale=ctx.locale),
            value=user.created_at.replace(microsecond=0),
            inline=False,
        )

        if ctx.guild is not None and isinstance(user, discord.Member):
            # Filter out the @everyone role.
            roles = [r for r in user.roles if r.name != "@everyone"]

            embed.add_field(
                name=_("Joined", locale=ctx.locale),
                value=user.joined_at.replace(microsecond=0),
                inline=False,
            )
            if len(roles) > 0:
                embed.add_field(
                    name=_("Roles", locale=ctx.locale),
                    value=", ".join(r.name for r in roles),
                    inline=False,
                )

        try:
            await ctx.channel.send(embed=embed)
        except (
            discord.errors.CommandInvokeError,
            discord.errors.HTTPException,
        ):
            await ctx.channel.send(
                _("Failed to find account information.", locale=ctx.locale)
            )

    @dcommands.command(
        doc=CommandDocumentation(
            command="iam",
            aliases=["give_role"],
            doc=N_("Self-assign a role."),
            args=[
                ArgumentDocumentation(
                    name="role_name",
                    doc=N_("The role to assign."),
                )
            ],
        ),
        aliases=["give_role"],
    )
    @dcommands.guild_only()
    async def iam(self, ctx: ContextBase, *, role: discord.Role):
        cfg: ServerConfig = ctx.bot.get_module_config(
            ctx.guild.id, self.qualified_name
        )

        if cfg is None or role.id not in cfg.iam_roles:
            await ctx.send(
                _(
                    "`{role.name}` is not on the list of self-assignable"
                    " roles.",
                    locale=ctx.locale,
                ).format(role=role)
            )
        else:
            await ctx.author.add_roles(role)

            await ctx.send(
                _(
                    "`{role.name}` has been assigned to `{author.display_name}`",
                    locale=ctx.locale,
                ).format(role=role, author=ctx.author),
                delete_after=10,
            )

    @dcommands.command(
        doc=CommandDocumentation(
            command="iamnot",
            aliases=["take_role"],
            doc=N_("Remove a self-assigned role."),
            args=[
                ArgumentDocumentation(
                    name="role_name",
                    doc=N_("The role to unassign."),
                )
            ],
        ),
        aliases=["take_role"],
    )
    @dcommands.guild_only()
    async def iamnot(self, ctx: ContextBase, *, role: discord.Role):
        cfg: ServerConfig = ctx.bot.get_module_config(
            ctx.guild.id, self.qualified_name
        )

        if cfg is None or role.id not in cfg.iam_roles:
            await ctx.send(
                _(
                    "`{role.name}` is not on the list of self-assignable"
                    " roles.",
                    locale=ctx.locale,
                ).format(role=role)
            )
        else:
            await ctx.author.remove_roles(role)

            await ctx.send(
                _(
                    "`{role.name}` has been removed from `{author.display_name}`",
                    locale=ctx.locale,
                ).format(role=role, author=ctx.author),
                delete_after=10,
            )

    @dcommands.group(
        doc=GroupDocumentation(
            command="test",
            doc=N_("Test Command"),
        ),
    )
    async def test(self, ctx: ContextBase, string: Union[DateTime, Duration]):
        await ctx.send(f"```\n{type(string)}: {string}\n```")

    @test.command(
        doc=CommandDocumentation(command="subtest", doc=N_("Subtest Command"))
    )
    async def subtest(self, ctx: ContextBase):
        pass


def setup(bot: Application):
    bot.add_module(General(bot))


def teardown(bot: Application):
    bot.remove_module(General.qualified_name)
