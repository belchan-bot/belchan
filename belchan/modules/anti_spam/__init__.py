from typing import Optional

from belchan.application import Application
from belchan.models.config import ModuleConfig
from belchan.modules import Module
from belchan.i18n import _, lazy_gettext as N_


class AntiSpamConfig(ModuleConfig):
    allow_invite_links: Optional[bool] = True


class AntiSpam(Module, doc=N_("Anti-Spam features"), config=AntiSpamConfig):
    pass


def setup(bot: Application):
    ext = AntiSpam(bot)

    bot.add_module(ext)


def teardown(bot: Application):
    bot.remove_module(AntiSpam)
