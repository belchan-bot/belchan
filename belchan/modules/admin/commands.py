from __future__ import annotations
from typing import Optional, TYPE_CHECKING, Union

import datetime
import json

import discord
from discord.errors import Forbidden

from belchan import converters
from belchan.models.permissions import MissingBotPermission
from belchan.models.config import MissingConfiguration
from belchan.models.database import CommandLog, DoesNotExist
from belchan.database import db_session, db_context
from belchan.i18n import _
from .database import AutoBan, Ban, Mute, Warning

if TYPE_CHECKING:
    from belchan.application import Application
    from . import AdminConfig


@db_session
async def ban(
    app: Application,
    config: AdminConfig,
    locale: str,
    server: Union[discord.Guild, int],
    user: Union[discord.User, discord.Member, int],
    by: Union[discord.User, discord.Member, int],
    reason: str,
    delete_message_days: Optional[int] = 0,
    until: Optional[Union[datetime.timedelta, datetime.datetime]] = None,
    autoban: Optional[bool] = False,
) -> Ban:
    """Bans a user from a server.

    Args:
        app: The bot instance.
        server: The server to ban the user from.
        user: The user to ban from the server.
        by: The user who invoked the ban.
        reason: The reason for the ban.
        delete_message_days: How many days of messages should deleted. Max: 7.
            If the value is 0, no messages will be deleted.
        config: The configuration instance for the Admin module.
            If not set, no log message will be generated and the user will not
            be notified via DM.
        until: Until when the ban should be active.
            After this point in time, the bot will automatically unban the
            user.

            NOTE: If the value is none, the user will never be automatically
                unbanned.
        autoban: If this is set to true, the ban will be marked as performed
            automatically.
    """
    server: discord.Guild = await converters.to_server(app, server)
    user: discord.Member = await converters.to_user_or_member(app, user, server)
    by: discord.User = await converters.to_user_or_member(app, user, server)

    if isinstance(until, datetime.timedelta):
        until = datetime.datetime.now() + until

    try:
        await user.ban(reason=reason, delete_message_days=delete_message_days)
    except Forbidden:
        raise MissingBotPermission("ban")

    db_entry: Ban = Ban.create(
        user_id=user.id,
        server_id=server.id,
        by=by.id,
        reason=reason,
        until=until,
    )

    ##################
    # Logging and DMs

    CommandLog.create(
        server_id=server.id,
        command="ban",
        user=by.id,
        time=datetime.datetime.now(),
        _args=json.dumps(
            {
                "user": user.id,
                "reason": reason,
                "until": until,
                "delete_message_days": delete_message_days,
            }
        ),
    )

    dm: discord.Embed = discord.Embed(
        title=config.ban_dm_title.format(
            user=user, reason=reason, until=until, server=server
        ),
        description=config.ban_dm_content.format(
            user=user, reason=reason, until=until, server=server
        ),
        color=0xFF0000,
    )
    dm.add_field(name="Until", value=until)

    dm_sent = False

    try:
        await user.send(embed=dm)

        dm_sent = True
    except Exception:
        pass

    if config.warning_log_channel is not None:
        channel: discord.TextChannel = await converters.to_channel(
            app, server, config.warning_log_channel
        )

        log_message: discord.Embed = discord.Embed(
            title=(
                _(
                    "User {user.display_name} ({user.id}) has been banned.",
                    locale,
                ).format(user=user)
            ),
            description=reason,
            color=0xFF0000,
        )
        log_message.add_field(
            name=_("By", locale), value=by.mention, inline=False
        )

        if until is not None:
            log_message.add_field(
                name=_("Until", locale),
                value=until.replace(
                    tzinfo=datetime.timezone.utc, microsecond=0
                ).isoformat(),
                inline=False,
            )

        log_message.add_field(
            name=_("Notification DM Received", locale),
            value=_("Yes", locale) if dm_sent else _("No", locale),
            inline=True,
        )

        if autoban:
            log_message.add_field(
                name="Autoban", value=_("Yes", locale), inline=True
            )

        await channel.send(embed=log_message)

    return db_entry


@db_session
async def unban(
    app: Application,
    config: Optional[AdminConfig],
    locale: str,
    server: Union[discord.Guild, int],
    user: Union[discord.User, discord.Member, int],
    by: Union[discord.User, discord.Member, int],
    reason: Optional[str] = None,
):
    """Unbans a user from a server.

    Args:
        app: The bot instance.
        server: The server to unban the user from.
        user: The user to unban from the server.
        by: The user who invoked the command.
        reason: The reason for lifting the ban.
        config: The configuration instance for the Admin module.
            If not set, no log message will be generated.

            NOTE: If the value is none, the user will never be automatically
                unbanned.
    """
    server: discord.Guild = await converters.to_server(app, server)
    user: discord.Member = await converters.to_user_or_member(app, user)

    try:
        Ban.get(
            Ban.user_id == user.id, Ban.server_id == server.id
        ).delete_instance()
    except DoesNotExist:
        pass

    await server.unban(user, reason=reason)

    ##################
    # Logging

    CommandLog.create(
        server_id=server.id,
        command="unban",
        user=by.id,
        time=datetime.datetime.now(),
        _args=json.dumps(
            {
                "server": server.id,
                "user": user.id,
                "reason": reason,
            }
        ),
    )

    if config.command_log_channel is not None:
        channel: discord.TextChannel = await converters.to_channel(
            app, server, config.command_log_channel
        )

        log_message: discord.Embed = discord.Embed(
            title=(
                _(
                    "User {user.display_name} ({user.id}) has been unbanned.",
                    locale,
                ).format(user=user)
            ),
            color=0xFF0000,
            description=reason,
        )
        log_message.add_field(name=_("By", locale), value=by.mention)

        await channel.send(embed=log_message)


@db_session
async def mute(
    app: Application,
    config: Optional[AdminConfig],
    locale: str,
    server: Union[discord.Guild, int],
    user: Union[discord.User, discord.Member, int],
    reason: str,
    by: Union[discord.User, discord.Member, int],
    until: Optional[Union[datetime.timedelta, datetime.datetime]] = None,
) -> Mute:
    """Mutes a user.

    Args:
        app: The bot instance.
        server: The server to ban the mute the server on.
        locale: The locale for the generated messages.
        user: The user to mute on the server.
        by: The user who invoked the mute.
        reason: The reason for the mute.
        config: The configuration instance for the Admin module.
            If not set, no log message will be generated and the user will not
            be notified via DM.
        until: Until when the mute should be active.
            After this point in time, the bot will automatically unmute the
            user.

            NOTE: If the value is none, the user will never be automatically
                unmuted.
    """
    if config.mute_role is None:
        raise MissingConfiguration("mute_role")

    if isinstance(until, datetime.timedelta):
        until = datetime.datetime.now() + until

    server: discord.Guild = await converters.to_server(app, server)
    user: discord.Member = await converters.to_user_or_member(app, user, server)
    by: discord.User = await converters.to_user_or_member(app, by, server)
    role: discord.Role = await converters.to_role(app, config.mute_role, server)

    await user.add_roles(role, reason=f"Bot-muted by: {by.name} (by.id)")

    mute: Mute = Mute.create(
        server_id=server.id,
        user_id=user.id,
        reason=reason,
        until=until,
        by=by.id,
    )

    ##################
    # Logging

    CommandLog.create(
        server_id=server.id,
        command="mute",
        user=by.id,
        time=datetime.datetime.now(),
        _args=json.dumps(
            {
                "server": server.id,
                "user": user.id,
                "reason": reason,
            }
        ),
    )

    if config.command_log_channel is not None:
        channel: discord.TextChannel = await converters.to_channel(
            app, server, config.command_log_channel
        )

        log_message: discord.Embed = discord.Embed(
            title=(
                _(
                    "User {user.display_name} ({user.id}) has been muted.",
                    locale,
                ).format(user=user)
            ),
            color=0x0000FF,
            description=reason,
        )
        log_message.add_field(name=_("By", locale=locale), value=by.mention)

        if until is not None:
            log_message.add_field(
                name=_("Until", locale=locale),
                value=until.replace(
                    tzinfo=datetime.timezone.utc, microsecond=0
                ).isoformat(),
            )

        await channel.send(embed=log_message)

    return mute


@db_session
async def unmute(
    app: Application,
    config: Optional[AdminConfig],
    locale: str,
    server: Union[discord.Guild, int],
    user: Union[discord.User, discord.Member, int],
    reason: str,
    by: Union[discord.User, discord.Member, int],
):
    """Umutes a user.

    Args:
        app: The bot instance.
        server: The server to ban the mute the server on.
        user: The user to mute on the server.
        by: The user who invoked the mute.
        reason: The reason for the mute.
        config: The configuration instance for the Admin module.
            If not set, no log message will be generated and the user will not
            be notified via DM.
        locale: The locale for the generated messages.
    """
    if config.mute_role is None:
        raise MissingConfiguration("mute_role")

    server: discord.Guild = await converters.to_server(app, server)
    user: discord.Member = await converters.to_user_or_member(app, user, server)
    by: discord.User = await converters.to_user_or_member(app, by, server)
    role: discord.Role = await converters.to_role(app, config.mute_role, server)

    await user.remove_roles(role, reason=f"Bot-muted by: {by.name} (by.id)")

    ##################
    # Logging

    CommandLog.create(
        server_id=server.id,
        command="unmute",
        user=by.id,
        time=datetime.datetime.now(),
        _args=json.dumps(
            {
                "server": server.id,
                "user": user.id,
                "reason": reason,
            }
        ),
    )

    if config.command_log_channel is not None:
        channel: discord.TextChannel = await converters.to_channel(
            app, server, config.command_log_channel
        )

        log_message: discord.Embed = discord.Embed(
            title=(
                _(
                    "User {user.display_name} ({user.id}) has been unmuted.",
                    locale,
                ).format(user=user)
            ),
            color=0x0000FF,
            description=reason,
        )
        log_message.add_field(name=_("By", locale=locale), value=by.mention)

        await channel.send(embed=log_message)


@db_session
async def warn(
    app: Application,
    locale: str,
    server: Union[discord.Guild, int],
    user: Union[discord.User, discord.Member, int],
    by: Union[discord.User, discord.Member, int],
    reason: str,
    config: Optional[AdminConfig],
) -> Warning:
    """Warn a user.

    Args:
        app: The bot instance.
        locale: The locale for the generated messages.
        server: The server to warn the user on.
        user: The user to warn.
        reason: The reason for the warning. The user will be able to see this.
        config: The configuration instance for the Admin module.
            If not set, no log message will be generated and the user will not
            be notified via DM.
    """
    server: discord.Guild = await converters.to_server(app, server)
    user: discord.Member = await converters.to_user_or_member(app, user, server)
    by: discord.User = await converters.to_user_or_member(app, by, server)

    warning: Warning = Warning.create(
        user_id=user.id, server_id=server.id, by=by.id, reason=reason
    )

    ##################
    # Logging and DMs

    CommandLog.create(
        server_id=server.id,
        command="warn",
        user=by.id,
        time=datetime.datetime.now(),
        _args=json.dumps(
            {
                "server": server.id,
                "user": user.id,
                "reason": reason,
                "warning": warning.id,
            }
        ),
    )

    total_warnings: int = (
        Warning.select()
        .where(Warning.user_id == user.id, Warning.server_id == server.id)
        .count()
    )

    if config.warning_duration is not None:
        current_warnings: int = (
            Warning.select()
            .where(
                Warning.user_id == user.id,
                Warning.server_id == server.id,
                (
                    Warning.date
                    > (datetime.datetime.now() - config.warning_duration)
                ),
            )
            .count()
        )
    else:
        current_warnings = total_warnings

    dm: discord.Embed = discord.Embed(
        title=config.warning_dm_title.format(
            user=user, reason=reason, server=server
        ),
        description=config.warning_dm_content.format(
            user=user, reason=reason, server=server
        ),
        color=0xFF9900,
    )
    dm.add_field(name=_("Server", locale), value=server.name)

    dm_sent = False

    try:
        await user.send(embed=dm)

        dm_sent = True
    except Exception:
        pass

    if config.command_log_channel is not None:
        channel: discord.TextChannel = await converters.to_channel(
            app, server, config.warning_log_channel
        )

        log_message: discord.Embed = discord.Embed(
            title=(
                _(
                    "User {user.display_name} ({user.id}) has been issued a"
                    " warning.",
                    locale,
                ).format(user=user)
            ),
            color=0xFF9900,
            description=reason,
        )
        log_message.add_field(name="ID", value=warning.id, inline=True)
        log_message.add_field(
            name=_("By", locale), value=by.mention, inline=False
        )
        log_message.add_field(
            name=_("Current Warnings", locale),
            value=current_warnings,
            inline=True,
        )
        log_message.add_field(
            name=_("Total Warnings", locale),
            value=total_warnings,
            inline=True,
        )
        log_message.add_field(
            name=_("Notification DM Received", locale),
            value=_("Yes", locale) if dm_sent else _("No", locale),
            inline=False,
        )

        if channel is None:
            return

        await channel.send(embed=log_message)

        if (
            config.warning_limit_active is not None
            and current_warnings >= config.warning_limit_active
            and config.warning_limit_active is not None
            and config.warning_limit_active != 0
        ):
            await channel.send(_("Active warning limit reached.", locale))
            await ban(
                app=app,
                config=config,
                locale=locale,
                server=server,
                user=user,
                by=app.user,
                reason=_("Active warning limit reached.", locale),
                autoban=True,
            )
        elif (
            config.warning_limit_total is not None
            and total_warnings >= config.warning_limit_total
            and config.warning_limit_total is not None
            and config.warning_limit_total != 0
        ):
            await channel.send(_("Total warning limit reached.", locale))
            await ban(
                app=app,
                config=config,
                locale=locale,
                server=server,
                user=user,
                by=app.user,
                reason=_("Total warning limit reached.", locale),
                autoban=True,
            )

    return warning


@db_session
async def delete_warning(
    app: Application,
    config: AdminConfig,
    locale: str,
    server: Union[discord.Guild, int],
    warning_id: int,
    by: Union[discord.Member, discord.User, int],
    reason: str,
):
    """Removes a warning from a user.

    Args:
        app: The bot instance.
        config: The configuration instance for the Admin module.
            If not set, no log message will be generated and the user will not
            be notified via DM.
        locale: The locale for the generated messages.
        server: The server to warn the user on.
        warning_id: The ID of the warning to remove.
        by: The user who invoked the command.
        reason: The reason for the warning. The user will be able to see this.
    """
    server: discord.Guild = await converters.to_server(app, server)
    by: discord.User = await converters.to_user_or_member(app, by, server)
    warning: Warning = Warning.get(Warning.id == warning_id)
    user: discord.Member = await converters.to_user_or_member(
        app, warning.user_id, server
    )

    try:
        warning.delete_instance()
    except DoesNotExist:
        pass

    ##################
    # Logging

    with db_session():
        CommandLog.create(
            server_id=server.id,
            command="warn",
            user=by.id,
            time=datetime.datetime.now(),
            _args=json.dumps(
                {
                    "server": server.id,
                    "warning": warning_id,
                    "user": user.id,
                    "reason": reason,
                }
            ),
        )

    if config.command_log_channel is not None:
        channel: discord.TextChannel = await converters.to_channel(
            app, server, config.warning_log_channel
        )

        log_message: discord.Embed = discord.Embed(
            title=(
                _(
                    "Warning ID {warning_id} for user {user.display_name}"
                    " ({user.id}) has been deleted.",
                    locale,
                ).format(warning_id=warning_id, user=user)
            ),
            color=0xFF9900,
            description=reason,
        )
        log_message.add_field(name=_("By", locale), value=by.mention)

        await channel.send(embed=log_message)

    return warning


async def role_add(
    app: Application,
    server: Union[discord.Guild, int],
    user: Union[discord.Member, int],
    role: Union[discord.Role, int],
    by: Union[discord.Member, discord.User, int],
):
    """Adds a role to a user.

    Args:
        app: The bot instance.
        config: The configuration instance for the Admin module.
            If not set, no log message will be generated and the user will not
            be notified via DM.
        locale: The locale for the generated messages.
        server: The server to add the role on.
        user: The user to add the role to.
        role: The role to add.
        by: The user who invoked the command.
    """
    server: discord.Guild = await converters.to_server(app, server)
    by: discord.User = await converters.to_user_or_member(app, by, server)
    role: discord.Role = await converters.to_role(app, role, server)
    user: discord.Member = await converters.to_user_or_member(app, user, server)

    await user.add_roles(role)

    ##################
    # Logging

    with db_context():
        CommandLog.create(
            server_id=server.id,
            command="role_add",
            user=by.id,
            time=datetime.datetime.now(),
            _args=json.dumps(
                {
                    "server": server.id,
                    "user": user.id,
                    "role": role.id,
                }
            ),
        )


async def role_remove(
    app: Application,
    server: Union[discord.Guild, int],
    user: Union[discord.Member, int],
    role: Union[discord.Role, int],
    by: Union[discord.Member, discord.User, int],
):
    """Adds a role to a user.

    Args:
        app: The bot instance.
        config: The configuration instance for the Admin module.
            If not set, no log message will be generated and the user will not
            be notified via DM.
        locale: The locale for the generated messages.
        server: The server to remove the role on.
        user: The user to remove the role from.
        role: The role to remove.
        by: The user who invoked the command.
    """
    server: discord.Guild = await converters.to_server(app, server)
    by: discord.User = await converters.to_user_or_member(app, by, server)
    role: discord.Role = await converters.to_role(app, role, server)
    user: discord.Member = await converters.to_user_or_member(app, user, server)

    await user.remove_roles(role)

    ##################
    # Logging

    async with db_context():
        CommandLog.create(
            server_id=server.id,
            command="role_remove",
            user=by.id,
            time=datetime.datetime.now(),
            _args=json.dumps(
                {
                    "server": server.id,
                    "user": user.id,
                    "role": role.id,
                }
            ),
        )


async def delete(
    app: Application,
    config: AdminConfig,
    locale: str,
    server: Union[discord.Guild, int],
    user: Union[discord.Member, int],
    channel: Union[discord.TextChannel, int],
    message: int,
    reason: str,
):
    server: discord.Guild = await converters.to_server(app, server)
    channel: discord.TextChannel = await converters.to_channel(
        app, server, channel
    )
    user: discord.Member = await converters.to_user_or_member(app, user, server)

    msg: discord.Message = channel.fetch_message(message)

    await msg.delete()

    ##################
    # Logging

    with db_context():
        CommandLog.create(
            server_id=server.id,
            command="delete",
            user=user.id,
            time=datetime.datetime.now(),
            _args=json.dumps(
                {
                    "server": server.id,
                    "channel": channel.id,
                    "message": message,
                }
            ),
        )

    log_channel = await converters.to_channel(
        app, server, config.command_log_channel
    )

    if log_channel is None:
        return

    embed = discord.Embed()
    embed.title = _("A message has been deleted.", locale=locale)
    embed.description = message.clean_content
    embed.add_field(
        name=_("Author", locale=locale),
        value=f"{message.author.display_name} ({message.author.id})",
        inline=True,
    )
    embed.add_field(
        name=_("Channel", locale=locale),
        value=f"{message.channel.mention}",
        inline=True,
    )
    embed.add_field(
        name=_("Date Posted", locale=locale),
        value=f"{message.created_at.replace(microsecond=0)}",
        inline=True,
    )
    embed.add_field(
        name=_("Date Deleted", locale=locale),
        value=datetime.datetime.now().replace(microsecond=0),
        inline=True,
    )
    embed.add_field(
        name=_("Deleted By", locale=locale),
        value=f"{message.author.display_name}" f" ({message.author.id})",
    )

    await log_channel.send(embed=embed)
