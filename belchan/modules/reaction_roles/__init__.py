from typing import Optional, List

import discord
from discord.partial_emoji import PartialEmoji

from belchan.application import Application
from belchan.context import ContextBase
from belchan.i18n import lazy_gettext as N_, _
from belchan import commands as dcommands
from belchan.models.docs import (
    ArgumentDocumentation,
    CommandDocumentation,
)
from belchan.models.enum import Enum
from belchan.modules import Module
from belchan import converters

from . import database, commands
from .types import AnyEmoji


class ModeEnum(str, Enum):
    exclusive: str = "exclusive"
    delete: str = "delete"


class ReactionRoles(
    Module,
    doc=N_("Reaction based role assignment."),
    database_tables=[database.ReactionRole, database.ReactionRoleMessage],
):
    @dcommands.command(
        doc=CommandDocumentation(
            command="reaction_role_add",
            doc=N_("Adds a role selection reaction to a message."),
            args=[
                ArgumentDocumentation(
                    name="channel", doc=N_("The channel the message is in.")
                ),
                ArgumentDocumentation(
                    name="message", doc=N_("The ID of the message.")
                ),
                ArgumentDocumentation(
                    name="reaction",
                    doc=N_("The reaction to add to the message."),
                ),
                ArgumentDocumentation(
                    name="role", doc=N_("The role the reaction will assign.")
                ),
                ArgumentDocumentation(
                    name="mode",
                    doc=N_(
                        "The mode of the message. `exclusive` to only allow one"
                        " choice or `delete` to delete added reactions."
                    ),
                    optional=True,
                ),
            ],
        ),
        aliases=["rra"],
    )
    async def reaction_role_add(
        self,
        ctx: ContextBase,
        channel: discord.TextChannel,
        message: int,
        reaction: AnyEmoji,
        role: discord.Role,
        mode: ModeEnum,
    ):
        exclusive: bool = mode == ModeEnum.exclusive
        delete: bool = mode == ModeEnum.delete

        await commands.reaction_role_add(
            self.bot,
            ctx.author.id,
            ctx.guild.id,
            channel.id,
            message,
            reaction,
            role.id,
            exclusive=exclusive,
            delete=delete,
        )

        await ctx.channel.send(_("Reaction role added", locale=ctx.locale))

    @dcommands.command(
        doc=CommandDocumentation(
            command="reaction_role_delete",
            doc=N_("Removes a role selection reaction from a message."),
        ),
    )
    async def reaction_role_remove(
        self,
        ctx: ContextBase,
        channel: discord.TextChannel,
        message_id: int,
        reaction: Optional[AnyEmoji] = None,
    ):
        try:
            await commands.reaction_role_remove(
                self.bot,
                ctx.locale,
                ctx.author.id,
                ctx.guild.id,
                channel.id,
                message_id,
                reaction,
            )
        except Exception as exc:
            await ctx.send(exc)

    @dcommands.Cog.listener()
    async def on_raw_reaction_add(
        self, payload: discord.RawReactionActionEvent
    ):
        # Ignore the bot itself.
        if payload.user_id == self.bot.user.id:
            return

        if not database.is_reaction_role_message(payload.message_id):
            return

        message_entry = database.ReactionRoleMessage.get_or_none(
            message_id=payload.message_id
        )

        if message_entry is None:
            return

        server: discord.Guild = await converters.to_server(
            self.bot, payload.guild_id
        )
        message: discord.Message = await (
            await converters.to_channel(self.bot, server, payload.channel_id)
        ).fetch_message(payload.message_id)
        member: discord.Member = server.get_member(payload.user_id)

        role: discord.Role = None
        roles: List[discord.Role] = []

        for reaction in message_entry.reactions:
            roles.append(message.guild.get_role(reaction.role_id))

            if (
                reaction.emoji_id == payload.emoji.id
                and reaction.emoji_id is not None
            ) or (
                reaction.emoji_name == payload.emoji.name
                and reaction.emoji_name is not None
            ):
                role = server.get_role(reaction.role_id)

        await member.add_roles(role)

        if message_entry.exclusive:
            # Remove other roles selectable from the current role message.
            await member.remove_roles(
                *[
                    r
                    for r in roles
                    if r.id != role.id
                    and r.id in [mr.id for mr in member.roles]
                ]
            )

            for reaction in message_entry.reactions.select().where(
                database.ReactionRole.role_id != role.id
            ):
                await message.remove_reaction(
                    PartialEmoji(
                        id=reaction.emoji_id, name=reaction.emoji_name
                    ),
                    member,
                )

        if message_entry.delete_reaction:
            await message.remove_reaction(
                PartialEmoji(id=reaction.emoji_id, name=reaction.emoji_name),
                member,
            )

    @dcommands.Cog.listener()
    async def on_raw_reaction_remove(
        self, payload: discord.RawReactionActionEvent
    ):
        # Ignore the bot itself.
        if payload.user_id == self.bot.user.id:
            return

        if not database.is_reaction_role_message(payload.message_id):
            return

        role_entries = database.ReactionRole.select().where(
            database.ReactionRole.message_id == payload.message_id,
            (
                (database.ReactionRole.emoji_id == payload.emoji.id)
                | (database.ReactionRole.emoji_name == payload.emoji.name)
            ),
        )

        if len(role_entries) == 0:
            return

        message_configs = database.ReactionRoleMessage.select().where(
            database.ReactionRoleMessage.message_id == payload.message_id
        )

        # If the message is set to delete reactions, ignore this event.
        # Otherwise the role the user just got assigned will be removed again.
        if len(message_configs) > 0 and message_configs[0].delete_reaction:
            return

        server: discord.Guild = self.bot.get_guild(payload.guild_id)
        member: discord.Member = server.get_member(payload.user_id)

        role: discord.Role = server.get_role(role_entries[0].role_id)

        try:
            await member.remove_roles(role)
        except discord.HTTPException:
            pass


def setup(bot: Application):
    ext = ReactionRoles(bot)

    bot.add_module(ext)


def teardown(bot: Application):
    bot.remove_module(ReactionRoles.qualified_name)
