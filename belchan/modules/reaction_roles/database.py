from belchan.models.database import (
    BooleanField,
    IntegerField,
    Model,
    TextField,
    ForeignKeyField,
)
from belchan.database import db_session

from ...cache import cached, TTLCache


class ReactionRoleMessage(Model):
    id: int = IntegerField(primary_key=True, unique=True)
    exclusive: bool = BooleanField(default=False)
    delete_reaction: bool = BooleanField(default=False)
    server_id: int = IntegerField()
    channel_id: int = IntegerField()
    message_id: int = IntegerField(unique=True)

    class Meta:
        indexes = ((("server_id", "channel_id", "message_id"), True),)


class ReactionRole(Model):
    id: int = IntegerField(primary_key=True, unique=True)
    message_id: int = ForeignKeyField(
        ReactionRoleMessage,
        backref="reactions",
        field="message_id",
    )
    emoji_id: int = IntegerField(null=True)
    emoji_name: str = TextField(null=True)
    role_id: int = IntegerField()

    class Meta:
        indexes = (
            (("message_id", "role_id"), True),
            (("message_id", "emoji_id"), True),
            (("message_id", "emoji_name"), True),
        )


cache: TTLCache = TTLCache(maxsize=8000, ttl=3600)


@cached(cache=cache)
@db_session
def is_reaction_role_message(message_id: int) -> bool:
    return (
        ReactionRoleMessage.select()
        .where(ReactionRoleMessage.message_id == message_id)
        .count()
        >= 1
    )
