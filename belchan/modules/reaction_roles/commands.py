from typing import Union, Optional

import discord

from belchan import converters
from belchan.database import db_session
from belchan.application import Application
from belchan.models.database import CommandLog
from belchan.i18n import _

import emoji

from .types import AnyEmoji
from .database import ReactionRole, ReactionRoleMessage


@db_session
async def reaction_role_add(
    app: Application,
    user_id: int,
    server_id: int,
    channel_id: int,
    message_id: int,
    emote: AnyEmoji,
    role_id: int,
    exclusive: bool,
    delete: bool,
):
    channel: discord.TextChannel = await converters.to_channel(
        app, server_id, channel_id
    )
    message: discord.Message = await channel.fetch_message(message_id)

    message_entry, created = ReactionRoleMessage.get_or_create(
        server_id=server_id, channel_id=channel_id, message_id=message_id
    )

    message_entry.exclusive = exclusive
    message_entry.delete_reaction = delete

    if message_entry.is_dirty:
        message_entry.save()

    reaction_entry = ReactionRole.get_or_create(
        message_id=message_id, role_id=role_id
    )[0]

    emote_: Union[int, str] = None

    if isinstance(emote, str):
        reaction_entry.emoji_name = emoji.emojize(emote)
        emote_ = emoji.emojize(emote)
    else:  # discord.Emoji or discord.PartialEmoji
        reaction_entry.emoji_id = emote.id
        emote_ = emote.id

    reaction_entry.save()

    await message.add_reaction(emote)

    log_entry = CommandLog.create(
        server_id=server_id, command="add_reaction_role", user=user_id
    )

    log_entry.args = {
        "channel": channel_id,
        "message": message_id,
        "emoji": emote_,
        "role": role_id,
        "exclusive": exclusive,
        "delete": delete,
    }

    log_entry.save()


@db_session
async def reaction_role_remove(
    app: Application,
    locale: str,
    user_id: int,
    server_id: int,
    channel_id: int,
    message_id: int,
    emote: Optional[AnyEmoji] = None,
):
    message: discord.Message = None
    channel: discord.TextChannel = await converters.to_channel(
        app, server_id, channel_id
    )
    message: discord.Message = await channel.fetch_message(message_id)

    if not (
        ReactionRoleMessage.select()
        .where(ReactionRoleMessage.message_id == message_id)
        .count()
    ):
        raise Exception(
            _(
                "Message {message} is not a reaction role message.",
                locale=locale,
            ).format(message=message_id)
        )

    if emote is None:
        ReactionRole.delete().where(
            ReactionRole.message_id == message_id
        ).execute()
        ReactionRoleMessage.delete().where(
            ReactionRoleMessage.message_id == message_id
        ).execute()
    else:
        if isinstance(emote, str):
            ReactionRole.delete().where(
                ReactionRole.message_id == message_id,
                ReactionRole.emoji_name == emote,
            ).execute()
        else:
            ReactionRole.delete().where(
                ReactionRole.message_id
                == message_id
                & (
                    ReactionRole.emoji_id
                    == emote.id | ReactionRole.emoji_name
                    == emote.name
                )
            ).execute()

            # Remove the message's reaction role configuration, if it has no
            # reaction to role assignments left.
        if (
            ReactionRoleMessage.get(message_id == message_id).reactions.count()
            == 0
        ):
            ReactionRoleMessage.delete().where(
                ReactionRoleMessage.message_id == message_id
            )

    try:
        message: discord.Message = await channel.fetch_message(message_id)
    except discord.NotFound:
        raise Exception(
            _(
                "Message {message} could not be found in channel {channel.mention}",
                locale=locale,
            ).format(message=message_id, channel=channel)
        )
    except discord.Forbidden:
        raise Exception(
            _("I'm not allowed to see channel {channel.mention}").format(
                channel=channel
            )
        )

    await message.clear_reaction(emote)

    log_entry = CommandLog.create(
        server_id=server_id, command="add_reaction_role", user=user_id
    )

    log_entry.args = {
        "channel": channel_id,
        "message": message_id,
        "emoji": emote,
    }

    log_entry.save()
