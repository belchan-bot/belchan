Development Environment Setup
#############################

Belchan is made to be hackable and has an easy way to get your development
environment set up and running.

First, make sure you already followed the installation instructions from
:ref:`user/getting_started:Getting Started` so you have the sources prepared and
a bot-account to test on.

Installing the Development Dependencies
=======================================

As written in :ref:`user/getting_started:Getting Started`, belchan uses
`Poetry <https://python-poetry.org/>`_ for managing the environment.
If you followed the instructions correctly, you installed the dependencies for
running belchan, but not for development.

There is two ways to install the development environment:

**Basic Environment**
  If you just want to program your own modules, you only need to install the
  basic development dependencies without the documentation tools.

**Full Environment**
  If you want to contribute to belchan (e.g. patches for official modules or
  propose a new official module), you should install the dependencies for
  the documentation and code-formatting as well.

.. tab:: Basic Environment

   .. code-block:: console

      > poetry install -E dev

.. tab:: Full Environment

   .. code-block:: console

      > poetry install -E contrib
