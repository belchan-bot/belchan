Background Tasks
================

Sometimes modules need to run scheduled background tasks. For this, the task
decorator from :mod:`belchan.modules` can be used.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   import datetime

   from belchan.modules import tasks

   class MyModule(Module, ...):
       @tasks.task(minutes=5) # every 5 minutes
       def log_time(self):
           self.bot.logger.info(datetime.now())

The task defined above will not run yet. For this, the ``start`` function of the
task has to be called.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   import datetime

   from belchan.application import Application
   from belchan.modules import tasks

   class MyModule(Module, ...):
       def __init__(self, bot: Application):
           self.log_time.start()

       # Started tasks need to be stopped when the module is unloaded,
       # otherwise they will just keep on running.
       #
       # For this the on_module_unload function can be overridden.
       def on_module_unload(self):
           self.log_time.stop()

       @tasks.task(minutes=5) # every 5 minutes
       async def log_time(self):
            await self.bot.logger.info(datetime.now())

Tasks run on the module level and do not have an accessible context. If you want
to access data from the discord API, you have to do so using
:mod:`belchan.context.ContextBase`.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   class MyModule(Module, ...):
       @tasks.task(hours=24)
       async def log_member_count(self):
           for s in self.bot.guilds:
               await self.logger.info(f"Server {s.name} ({s.id}) has {s.member_count} members.")
