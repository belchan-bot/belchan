Permissions
===========

To restrict who can run certain commands, you can either use role-based
permissions, discord permissions or a mixture of both.

You can add permission checks to commandy by using one of the two decorators:

Require the given permission:
    :func:`belchan.permissions.require_permission`

Require any permission out of the list:
    :func:`belchan.permissions.require_any_permission`

.. code-block:: python
   :linenos:
   :caption: __init__.py

   import discord

   from belchan.permissions import require_permission

   class MyModule(Module, ...):
       @command(doc=CommandDocumentation(command="ping", doc=N_("Returns `pong`")))
       @require_permission(discord.Permissions.administrator)
       async def ping(self, ctx: ContextBase):
           await ctx.send("pong")

To require multiple permissions for a command, simply add the
:func:`belchan.permissions.require_permission` decorator multiple times, each
with a different permissions to check.

Defining New Permissions
------------------------

To define a new permission for your module, define a
:class:`belchan.models.permissions.ModulePermission` and add it to your module's
permission table.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   from belchan.models.permissions import ModulePermission

   class MyModule(Module, doc=ModuleDocumentation(name="My Module", doc=N_("Tutorial bot module.")),
                  permissions=[
                      ModulePermission(
                          name="my_ping_permission",
                          description=N_("Allows using the ping command."),
                      )
                  ]):
       pass

The newly created permission can now be referenced by its name in the permission
decorators.

.. note:: Permission names should be all lowercase.

.. note:: Prefix permissions of non-default modules with the module's name to
   avoid name conflicts.

   .. code-block::
      :caption: Example

      ModuleDocumentation(name="mymodule.ping", doc=N_("Allows using the ping command."))
