Database
========

For database connectivity and data handling, belchan uses `peewee
<http://docs.peewee-orm.com/en/latest/index.html>`_.

Table Definition
----------------

To define a database table, create a new class that inherits from
:class:`belchan.models.database.Model`.

.. code-block:: python
   :caption: database.py
   :linenos:
   :empasize-lines: 7

   from belchan.models.database import Model, IntegerField, TextField

   class MyDatabaseTable(Model):
      id = IntegerField(primary_key=True)
      sometield = TextField()

Modules can automatically create missing database tables from models.

.. code-block:: python
   :linenos:
   :caption: __init__.py

   from belchan.modules import Module

   from .database import MyDatabasetable

   # Adding the table's model to `database_tables` will ensure the table exists when
   # the module is loaded.
   class MyModule(Module, doc=ModuleDocumentation(name="My Module", doc=N_("Tutorial bot module.")),
      database_tables=[MyTable]):
      pass

Querying
--------

To establish a database connection, use :data:`belchan.database.db_session`.
:data:`belchan.database.db_session` can be used either as a decorator for your
command or task, or within as a context manager.

.. tab:: As Decorator

   .. code-block:: python
      :linenos:

      from belchan.database import db_session, commit

      @command(...)
      @db_session
      async def your_command(self, ctx: ContextBase):
          # Do something with the database models here.

          # Database connection will be freed as soon as the command is over.

.. tab:: As Context Manager

   .. code-block:: python
      :linenos:

      from belchan.database import db_session, commit

      @command(...)
      async def your_command(self, ctx: ContextBase):
          with db_session: # Database connection is acquired here.
              # Do something with the database models here.

          # Database connection is freed here

Whenever possible, the context manager variant should be used to return database
connections back to the pool faster, thereby blocking other commands less.

For more information about how to use the database, you can reference the
`peewee documentation <http://docs.peewee-orm.com/en/latest/index.html>`_.
