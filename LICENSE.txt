Copyright 2021 André Fischer

This software is distributed under the Contribute Back License (CBL).

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute and to permit persons to whom the Software is furnished to
do so, subject to the following conditions:

- You may not change the license of this software.
- Any changes made to existing files MUST be made available to the
origin of this software as defined in the CONTRIBUTING or
CONTRIBUTING.txt file.
- Any files referenced in changes to existing files MUST be included
in your contribution and must adhere to the rules defined in the
CONTRIBUTING or CONTRIBUTING.txt file.
- The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
