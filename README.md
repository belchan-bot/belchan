<h1 align="center">Belchan</h2>

<hr />

<p>Simple to use, modular discord bot with builtin permissions and per-server, per-module configuration.</p>

<div align="center">
[Getting Started](https://belchan.readthedocs.io/user/getting_started.html)
[Documentation](https://belchan.readthedocs.io)
[Modules](https://belchan.readthedocs.io/modules)
[License](./LICENSE.txt)
</div>
